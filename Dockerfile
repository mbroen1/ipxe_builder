FROM alpine:latest
RUN apk add --no-cache git gcc g++ binutils make perl mtools xorriso syslinux xz-dev bash py3-pip openssh openssl && \
    pip install j2cli[yaml]
